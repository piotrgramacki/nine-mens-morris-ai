package controllers;

import ai.heuristics.BoardEvaluationHeuristic;
import board.Field;
import board.utils.BoardCreator;
import engine.GameController;
import engine.GameObserver;
import engine.utils.*;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class BoardController implements Initializable, GameObserver {
    private HashMap<Circle, Field> fieldsMapping;

    private GameController gameController;

    private boolean gameStateChanged = false;

    @FXML
    private Circle field00;
    @FXML
    private Circle field03;
    @FXML
    private Circle field06;
    @FXML
    private Circle field11;
    @FXML
    private Circle field13;
    @FXML
    private Circle field15;
    @FXML
    private Circle field22;
    @FXML
    private Circle field23;
    @FXML
    private Circle field24;
    @FXML
    private Circle field30;
    @FXML
    private Circle field31;
    @FXML
    private Circle field32;
    @FXML
    private Circle field34;
    @FXML
    private Circle field35;
    @FXML
    private Circle field36;
    @FXML
    private Circle field42;
    @FXML
    private Circle field43;
    @FXML
    private Circle field44;
    @FXML
    private Circle field51;
    @FXML
    private Circle field53;
    @FXML
    private Circle field55;
    @FXML
    private Circle field60;
    @FXML
    private Circle field63;
    @FXML
    private Circle field66;
    @FXML
    private ChoiceBox<String> player1;
    @FXML
    private ChoiceBox<String> player2;
    @FXML
    private ChoiceBox<String> heuristic1;
    @FXML
    private ChoiceBox<String> heuristic2;
    @FXML
    private Button startButton;
    @FXML
    private CheckBox logging;
    @FXML
    private Label stageLabel;
    @FXML
    private Label playerLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fieldsInit();
        choiceBoxesInit();
        setupStartButton();
    }

    private void fieldsInit() {
        Map<Integer, Field> fields = BoardCreator.getFields();

        fieldsMapping = new HashMap<>() {{
            put(field00, fields.get(0));
            put(field03, fields.get(3));
            put(field06, fields.get(6));
            put(field11, fields.get(11));
            put(field13, fields.get(13));
            put(field15, fields.get(15));
            put(field22, fields.get(22));
            put(field23, fields.get(23));
            put(field24, fields.get(24));
            put(field30, fields.get(30));
            put(field31, fields.get(31));
            put(field32, fields.get(32));
            put(field34, fields.get(34));
            put(field35, fields.get(35));
            put(field36, fields.get(36));
            put(field42, fields.get(42));
            put(field43, fields.get(43));
            put(field44, fields.get(44));
            put(field51, fields.get(51));
            put(field53, fields.get(53));
            put(field55, fields.get(55));
            put(field60, fields.get(60));
            put(field63, fields.get(63));
            put(field66, fields.get(66));
        }};

        fieldsMapping.forEach((circle, field) -> circle.setOnMouseClicked(c -> {
            if (gameController != null) {
                gameController.fieldSelected(field);
            }
        }));

        updateFields();

        initGameLoop();
    }

    private void initGameLoop() {
        Runnable makeAction = () -> gameController.makeNextMove();

        Timeline gameLoopTimeline = new Timeline(
                new KeyFrame(
                        Duration.millis(100),
                        event -> {
                            if (gameStateChanged) {
                                gameStateChanged = false;
                                updateFields();

                                EventQueue.invokeLater(makeAction);
                            }
                        }
                )
        );
        gameLoopTimeline.setCycleCount( Animation.INDEFINITE );
        gameLoopTimeline.play();
    }

    @Override
    public void stateChanged() {
        gameStateChanged = true;
    }

    private void updateFields() {
        fieldsMapping.forEach((circle, field) -> {
            setFieldState(circle, field.getStatus());
            setFieldSelection(circle, field.getSelection());
        });

        if (gameController != null) {
            stageLabel.setText(Strings.getGameStage(gameController.getCurrentStage()));
            playerLabel.setText(Strings.getPlayer(gameController.getActivePlayer()));
        }
    }

    private void setFieldState(@NotNull Circle circle, FieldStatus status) {
        circle.setFill(Paint.valueOf(Colors.getFieldColor(status)));
    }

    private void setFieldSelection(@NotNull Circle circle, FieldSelection selection) {
        circle.setStroke(Paint.valueOf(Colors.getSelectionColor(selection)));
    }

    private void choiceBoxesInit() {
        addPlayerTypes(player1);
        addPlayerTypes(player2);
        addHeuristics(heuristic1);
        addHeuristics(heuristic2);
    }

    private void addHeuristics(@NotNull ChoiceBox<String> heuristic) {
        heuristic.getItems().addAll(Strings.HEURISTICS);
        heuristic.setValue(Strings.HEURISTICS.get(0));
    }

    private void addPlayerTypes(@NotNull ChoiceBox<String> player) {
        player.getItems().addAll(Strings.PLAYER_TYPES);
        player.setValue(Strings.PLAYER_TYPES.get(0));
    }

    private void setupStartButton() {
        startButton.setOnMouseClicked(event -> handleStart());
    }

    private void handleStart() {
        player1.setDisable(true);
        player2.setDisable(true);
        heuristic1.setDisable(true);
        heuristic2.setDisable(true);

        gameController = new GameController(
                PlayerType.getPlayerType(player1.getValue()),
                BoardEvaluationHeuristic.getHeuristic(heuristic1.getValue()),
                PlayerType.getPlayerType(player2.getValue()),
                BoardEvaluationHeuristic.getHeuristic(heuristic2.getValue()),
                logging.isSelected()
        );

        startButton.setDisable(true);

        gameController.registerObserver(this);
    }
}
