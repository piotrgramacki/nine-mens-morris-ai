package board.utils;

import board.Field;
import board.Mill;

import java.util.*;

/**
 * Creator for fields and mills on board
 */
public class BoardCreator {
    private static Map<Integer, Field> fields;
    private static Set<Mill> mills;

    /**
     * Returns all fields on board
     *
     * @return fields with their IDs
     */
    public static Map<Integer, Field> getFields() {
        if (fields == null) {
            createFields();
        }
        return fields;
    }

    /**
     * Returns all mills on board
     *
     * @return set of mills possible on board
     */
    public static Set<Mill> getMills() {
        if (mills == null) {
            createMills();
        }
        return mills;
    }

    private static void createFields() {
        fields = new HashMap<>() {{
            put(0, new Field(0, 0));
            put(3, new Field(0, 3));
            put(6, new Field(0, 6));
            put(11, new Field(1, 1));
            put(13, new Field(1, 3));
            put(15, new Field(1, 5));
            put(22, new Field(2, 2));
            put(23, new Field(2, 3));
            put(24, new Field(2, 4));
            put(30, new Field(3, 0));
            put(31, new Field(3, 1));
            put(32, new Field(3, 2));
            put(34, new Field(3, 4));
            put(35, new Field(3, 5));
            put(36, new Field(3, 6));
            put(42, new Field(4, 2));
            put(43, new Field(4, 3));
            put(44, new Field(4, 4));
            put(51, new Field(5, 1));
            put(53, new Field(5, 3));
            put(55, new Field(5, 5));
            put(60, new Field(6, 0));
            put(63, new Field(6, 3));
            put(66, new Field(6, 6));
        }};

        createNeighbours();
    }

    private static void createNeighbours() {
        // 1st row
        fields.get(0).addNeighbour(fields.get(3));
        fields.get(0).addNeighbour(fields.get(30));
        //
        fields.get(3).addNeighbour(fields.get(0));
        fields.get(3).addNeighbour(fields.get(6));
        fields.get(3).addNeighbour(fields.get(13));
        //
        fields.get(6).addNeighbour(fields.get(3));
        fields.get(6).addNeighbour(fields.get(36));

        // 2nd row
        fields.get(11).addNeighbour(fields.get(13));
        fields.get(11).addNeighbour(fields.get(31));
        //
        fields.get(13).addNeighbour(fields.get(3));
        fields.get(13).addNeighbour(fields.get(11));
        fields.get(13).addNeighbour(fields.get(15));
        fields.get(13).addNeighbour(fields.get(23));
        //
        fields.get(15).addNeighbour(fields.get(13));
        fields.get(15).addNeighbour(fields.get(35));

        // 3rd row
        fields.get(22).addNeighbour(fields.get(32));
        fields.get(22).addNeighbour(fields.get(23));
        //
        fields.get(23).addNeighbour(fields.get(22));
        fields.get(23).addNeighbour(fields.get(24));
        fields.get(23).addNeighbour(fields.get(13));
        //
        fields.get(24).addNeighbour(fields.get(23));
        fields.get(24).addNeighbour(fields.get(34));

        // 4th row
        fields.get(30).addNeighbour(fields.get(0));
        fields.get(30).addNeighbour(fields.get(60));
        fields.get(30).addNeighbour(fields.get(31));
        //
        fields.get(31).addNeighbour(fields.get(30));
        fields.get(31).addNeighbour(fields.get(11));
        fields.get(31).addNeighbour(fields.get(51));
        fields.get(31).addNeighbour(fields.get(32));
        //
        fields.get(32).addNeighbour(fields.get(22));
        fields.get(32).addNeighbour(fields.get(42));
        fields.get(32).addNeighbour(fields.get(31));
        //
        fields.get(34).addNeighbour(fields.get(24));
        fields.get(34).addNeighbour(fields.get(44));
        fields.get(34).addNeighbour(fields.get(35));
        //
        fields.get(35).addNeighbour(fields.get(15));
        fields.get(35).addNeighbour(fields.get(55));
        fields.get(35).addNeighbour(fields.get(34));
        fields.get(35).addNeighbour(fields.get(36));
        //
        fields.get(36).addNeighbour(fields.get(6));
        fields.get(36).addNeighbour(fields.get(66));
        fields.get(36).addNeighbour(fields.get(35));

        // 5th row
        fields.get(42).addNeighbour(fields.get(32));
        fields.get(42).addNeighbour(fields.get(43));
        //
        fields.get(43).addNeighbour(fields.get(42));
        fields.get(43).addNeighbour(fields.get(44));
        fields.get(43).addNeighbour(fields.get(53));
        //
        fields.get(44).addNeighbour(fields.get(43));
        fields.get(44).addNeighbour(fields.get(34));

        // 6th row
        fields.get(51).addNeighbour(fields.get(53));
        fields.get(51).addNeighbour(fields.get(31));
        //
        fields.get(53).addNeighbour(fields.get(63));
        fields.get(53).addNeighbour(fields.get(51));
        fields.get(53).addNeighbour(fields.get(55));
        fields.get(53).addNeighbour(fields.get(43));
        //
        fields.get(55).addNeighbour(fields.get(53));
        fields.get(55).addNeighbour(fields.get(35));

        // 7st row
        fields.get(60).addNeighbour(fields.get(63));
        fields.get(60).addNeighbour(fields.get(30));
        //
        fields.get(63).addNeighbour(fields.get(60));
        fields.get(63).addNeighbour(fields.get(66));
        fields.get(63).addNeighbour(fields.get(53));
        //
        fields.get(66).addNeighbour(fields.get(63));
        fields.get(66).addNeighbour(fields.get(36));
    }

    private static void createMills() {
        mills = new HashSet<>() {{
            add(new Mill(List.of(
                    fields.get(0),
                    fields.get(3),
                    fields.get(6)
            )));
            add(new Mill(List.of(
                    fields.get(11),
                    fields.get(13),
                    fields.get(15)
            )));
            add(new Mill(List.of(
                    fields.get(22),
                    fields.get(23),
                    fields.get(24)
            )));
            add(new Mill(List.of(
                    fields.get(30),
                    fields.get(31),
                    fields.get(32)
            )));
            add(new Mill(List.of(
                    fields.get(34),
                    fields.get(35),
                    fields.get(36)
            )));
            add(new Mill(List.of(
                    fields.get(42),
                    fields.get(43),
                    fields.get(44)
            )));
            add(new Mill(List.of(
                    fields.get(51),
                    fields.get(53),
                    fields.get(55)
            )));
            add(new Mill(List.of(
                    fields.get(60),
                    fields.get(63),
                    fields.get(66)
            )));
            add(new Mill(List.of(
                    fields.get(0),
                    fields.get(30),
                    fields.get(60)
            )));
            add(new Mill(List.of(
                    fields.get(11),
                    fields.get(31),
                    fields.get(51)
            )));
            add(new Mill(List.of(
                    fields.get(22),
                    fields.get(32),
                    fields.get(42)
            )));
            add(new Mill(List.of(
                    fields.get(3),
                    fields.get(13),
                    fields.get(23)
            )));
            add(new Mill(List.of(
                    fields.get(43),
                    fields.get(53),
                    fields.get(63)
            )));
            add(new Mill(List.of(
                    fields.get(24),
                    fields.get(34),
                    fields.get(44)
            )));
            add(new Mill(List.of(
                    fields.get(15),
                    fields.get(35),
                    fields.get(55)
            )));
            add(new Mill(List.of(
                    fields.get(6),
                    fields.get(36),
                    fields.get(66)
            )));
        }};
    }
}
