package board;

import engine.utils.FieldStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Mill {
    private ArrayList<Field> fields;

    public Mill(List<Field> fields) {
        this.fields = new ArrayList<>(fields);
    }

    /**
     * Checks mill with actual values of fields (the ones in engine)
     *
     * @return - is mill set
     */
    public boolean isSet() {
        return fields.stream().allMatch(field -> !field.isFree() && field.getStatus() == fields.get(0).getStatus());
    }

    /**
     * Checks mill for given board state
     *
     * @param boardState - state for which check should be made
     * @return - is mill set for given [boardState]
     */
    public boolean isSet(Map<Integer, FieldStatus> boardState) {
        return fields.stream().allMatch(field -> (boardState.get(field.getId()) != FieldStatus.FREE) &&
                        (boardState.get(fields.get(0).getId()) == boardState.get(field.getId())));
    }

    /**
     * Checks mill for given board state and player
     *
     * @param boardState - state for which check should be made
     * @param forPlayer - player for which should be checked
     * @return - is mill set for given boardState and player
     */
    public boolean isSet(Map<Integer, FieldStatus> boardState, FieldStatus forPlayer) {
        return fields.stream().allMatch(field -> boardState.get(field.getId()) == forPlayer);
    }

    /**
     * Checks if given move can set a mill
     *
     * @param fieldID - field to place which could potentially create mill
     * @param currentPlayer - player creating mill
     * @param boardState - current board state
     * @return - can placing on given field set this mill
     */
    public boolean isPossibleToSetWith(int fieldID, FieldStatus currentPlayer, Map<Integer, FieldStatus> boardState) {
        boolean res;

        if (fields.get(0).getId() == fieldID) {
            res = boardState.get(fields.get(1).getId()) == currentPlayer &&
                    boardState.get(fields.get(2).getId())  == currentPlayer;
        } else if (fields.get(1).getId() == fieldID) {
            res = boardState.get(fields.get(0).getId()) == currentPlayer &&
                    boardState.get(fields.get(2).getId()) == currentPlayer;
        } else {
            res = boardState.get(fields.get(0).getId()) == currentPlayer &&
                    boardState.get(fields.get(1).getId()) == currentPlayer;
        }

        return res;
    }

    /**
     * Checks if mill contains given field
     *
     * @param field - field to check
     */
    public boolean containsField(Field field) {
        return fields.contains(field);
    }

    /**
     * Checks if mill contains field with given ID
     *
     * @param fieldID - ID to check
     */
    public boolean containsField(int fieldID) {
        return fields.stream().anyMatch(field -> field.getId() == fieldID);
    }
}
