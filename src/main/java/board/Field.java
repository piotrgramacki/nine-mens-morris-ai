package board;

import engine.utils.FieldSelection;
import engine.utils.FieldStatus;
import org.jetbrains.annotations.Contract;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Field {
    private int id;
    private FieldStatus status = FieldStatus.FREE;
    private FieldSelection selection = FieldSelection.NONE;

    private Set<Field> neighbours;

    @Contract(pure = true)
    public Field(int xCoordinate, int yCoordinate) {
        this.id = xCoordinate * 10 + yCoordinate;
        neighbours = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public FieldStatus getStatus() {
        return status;
    }

    public void setStatus(FieldStatus status) {
        this.status = status;
    }

    public FieldSelection getSelection() {
        return selection;
    }

    public void setSelection(FieldSelection selection) {
        this.selection = selection;
    }

    public Set<Field> getNeighbours() {
        return neighbours;
    }

    public boolean isFree() {
        return status == FieldStatus.FREE;
    }

    public void addNeighbour(Field neighbour) {
        neighbours.add(neighbour);
    }

    public boolean isNeighbour(Field other) {
        return neighbours.contains(other);
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Field field = (Field) o;
        return id == field.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
