package engine.moves;

import engine.GameController;
import engine.utils.FieldStatus;

import java.util.Map;

public interface Move {
    int NO_REMOVE = -1;

    void execute(GameController engine);

    Map<Integer, FieldStatus> execute(Map<Integer, FieldStatus> boardStatus);

    boolean removes();
}
