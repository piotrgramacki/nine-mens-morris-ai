package engine.moves;

import board.Field;
import board.Mill;
import board.utils.BoardCreator;
import engine.moves.impl.MovingMove;
import engine.moves.impl.PlacingMove;
import engine.utils.FieldStatus;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class MovesGenerator {
    private Map<Integer, FieldStatus> boardState;

    public MovesGenerator(Map<Integer, FieldStatus> boardState) {
        this.boardState = boardState;
    }

    public List<Move> getPlacingMoves(FieldStatus currentPlayer) {
        List<Move> moves = new LinkedList<>();

        boardState.forEach((id, state) -> {
            if (state == FieldStatus.FREE) {
                if (checkMillsForPlacing(id, currentPlayer)) {
                    moves.addAll(generateRemovingPlacings(id, currentPlayer));
                } else {
                    moves.add(new PlacingMove(id, currentPlayer));
                }
            }
        });

        return moves;
    }

    public List<Move> getMovementMoves(FieldStatus currentPlayer) {
        List<Move> moves = new LinkedList<>();

        boardState.forEach((id, state) -> {
            if (state == currentPlayer) {
                moves.addAll(generateMovesFrom(id, currentPlayer));
            }
        });

        return moves;
    }

    private List<Move> generateMovesFrom(int fieldID, FieldStatus currentPlayer) {
        List<Move> moves = new LinkedList<>();

        getPossibleDestinations(fieldID, currentPlayer).forEach(neighbourID -> {
            if (boardState.get(neighbourID) == FieldStatus.FREE) {
                if (checkMillsForMoving(fieldID, neighbourID, currentPlayer)) {
                    moves.addAll(generateRemovingMoves(fieldID, neighbourID, currentPlayer));
                } else {
                    moves.add(new MovingMove(fieldID, neighbourID));
                }
            }
        });

        return moves;
    }

    private Stream<Integer> getPossibleDestinations(int startID, FieldStatus currentPlayer) {
        if (getPiecesCount(currentPlayer) == 3) {
            return boardState.keySet().stream();
        } else {
            Field field = BoardCreator.getFields().get(startID);

            return field.getNeighbours().stream().map(Field::getId);
        }
    }

    private int getPiecesCount(FieldStatus currentPlayer) {
        return (int) boardState.values().stream().filter(field -> field == currentPlayer).count();
    }

    private List<Move> generateRemovingPlacings(int placeID, FieldStatus currentPlayer) {
        List<Move> moves = new LinkedList<>();

        boardState.keySet().stream().filter(fieldID -> canRemoveField(fieldID, currentPlayer))
                .forEach(removeID -> moves.add(new PlacingMove(placeID, removeID, currentPlayer)));

        return moves;
    }

    private List<Move> generateRemovingMoves(int fromID, int toID, FieldStatus currentPlayer) {
        List<Move> moves = new LinkedList<>();

        boardState.keySet().stream().filter(fieldID -> canRemoveField(fieldID, currentPlayer))
                .forEach(removeID -> moves.add(new MovingMove(fromID, toID, removeID)));

        return moves;
    }

    private boolean checkMillsForPlacing(int fieldID, FieldStatus currentPlayer) {
        Set<Mill> mills = BoardCreator.getMills();

        return mills.stream().anyMatch(mill ->
                mill.containsField(fieldID) && mill.isPossibleToSetWith(fieldID, currentPlayer, boardState));
    }

    private boolean checkMillsForMoving(int from, int to, FieldStatus currentPlayer) {
        FieldStatus prevFrom = boardState.get(from);
        FieldStatus prevTo = boardState.get(to);

        boardState.replace(from, FieldStatus.FREE);
        boardState.replace(to, currentPlayer);

        Set<Mill> mills = BoardCreator.getMills();

        boolean result = mills.stream().anyMatch(mill -> mill.containsField(to) && mill.isSet(boardState));

        boardState.replace(from, prevFrom);
        boardState.replace(to, prevTo);

        return result;
    }

    private boolean canRemoveField(int fieldID, FieldStatus currentPlayer) {
        Set<Mill> mills = BoardCreator.getMills();

        return boardState.get(fieldID) == currentPlayer.opposite()
                && mills.stream().noneMatch(mill -> mill.containsField(fieldID) && mill.isSet(boardState));
    }
}
