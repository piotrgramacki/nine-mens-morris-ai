package engine.moves.impl;

import engine.GameController;
import engine.moves.Move;
import engine.utils.FieldStatus;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MovingMove implements Move {
    private int fieldFromID;
    private int fieldToID;

    private int thenRemove;

    public MovingMove(int fieldFromID, int fieldToID) {
        this(fieldFromID, fieldToID, NO_REMOVE);
    }

    public MovingMove(int fieldFromID, int fieldToID, int thenRemove) {
        this.fieldFromID = fieldFromID;
        this.fieldToID = fieldToID;
        this.thenRemove = thenRemove;
    }

    @Override
    public void execute(GameController engine) {
        engine.fieldSelected(fieldFromID);
        engine.fieldSelected(fieldToID);

        if (thenRemove != NO_REMOVE)
            engine.fieldSelected(thenRemove);
    }

    @Override
    public Map<Integer, FieldStatus> execute(Map<Integer, FieldStatus> boardStatus) {
        Map<Integer, FieldStatus> result = new HashMap<>(boardStatus);

        result.replace(fieldToID, result.get(fieldFromID));
        result.replace(fieldFromID, FieldStatus.FREE);

        if (thenRemove != NO_REMOVE) {
            result.replace(thenRemove, FieldStatus.FREE);
        }
        return result;
    }

    @Override
    public boolean removes() {
        return thenRemove != NO_REMOVE;
    }

    /**
     * Checks if moves are reverted (A->B == B->A)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovingMove that = (MovingMove) o;
        return fieldFromID == that.fieldToID &&
                fieldToID == that.fieldFromID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldFromID, fieldToID);
    }
}
