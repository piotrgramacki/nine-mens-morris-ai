package engine.moves.impl;

import engine.GameController;
import engine.moves.Move;
import engine.utils.FieldStatus;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class PlacingMove implements Move {
    private int fieldID;
    private FieldStatus current;

    private int thenRemove;

    public PlacingMove(int fieldID, FieldStatus current) {
        this(fieldID, NO_REMOVE, current);
    }

    public PlacingMove(int fieldID, int thenRemove, FieldStatus current) {
        this.fieldID = fieldID;
        this.current = current;
        this.thenRemove = thenRemove;
    }

    @Override
    public void execute(GameController engine) {
        engine.fieldSelected(fieldID);

        if (thenRemove != NO_REMOVE) {
            engine.fieldSelected(thenRemove);
        }
    }

    @Override
    public Map<Integer, FieldStatus> execute(Map<Integer, FieldStatus> boardStatus) {
        Map<Integer, FieldStatus> result = new HashMap<>(boardStatus);

        result.replace(fieldID, current);

        if (thenRemove != NO_REMOVE) {
            result.replace(thenRemove, FieldStatus.FREE);
        }

        return result;
    }

    @Override
    public boolean removes() {
        return thenRemove != NO_REMOVE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlacingMove that = (PlacingMove) o;
        return fieldID == that.fieldID &&
                thenRemove == that.thenRemove &&
                current == that.current;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldID, current, thenRemove);
    }
}
