package engine;

import ai.heuristics.BoardEvaluationHeuristic;
import ai.players.AlphaBetaPlayer;
import ai.players.MinMaxPlayer;
import ai.players.RandomPlayer;
import board.Field;
import board.Mill;
import board.utils.BoardCreator;
import engine.logs.ILogger;
import engine.logs.impl.FileLogger;
import engine.moves.Move;
import engine.moves.MovesGenerator;
import engine.utils.*;

import java.util.*;

public class GameController {
    private List<GameObserver> observers;

    private Map<Integer, IPlayer> players;

    private GameStage currentStage;
    private Field fieldToMove;
    private boolean finishedMoving = false;

    private Map<Integer, Field> fieldsIDs = BoardCreator.getFields();
    private Collection<Field> fields = fieldsIDs.values();
    private Set<Mill> mills = BoardCreator.getMills();

    private int fieldsToPlace = Rules.PIECES_PER_PLAYER * 2;

    private int activePlayer;

    private int movesCount = 1;

    private ILogger logger;

    public GameController(PlayerType player1,
                          BoardEvaluationHeuristic heuristic1,
                          PlayerType player2,
                          BoardEvaluationHeuristic heuristic2,
                          boolean loggingEnabled) {
        this.currentStage = GameStage.PLACING;
        this.activePlayer = 1;

        players = new HashMap<>() {{
            put(1, createPlayer(player1, FieldStatus.fromID(1), heuristic1));
            put(2, createPlayer(player2, FieldStatus.fromID(2), heuristic2));
        }};

        if (loggingEnabled) {
            logger = new FileLogger(createLogFileName(player1, heuristic1, player2, heuristic2));
        }

        this.observers = new LinkedList<>();
    }

    private String createLogFileName(PlayerType player1, BoardEvaluationHeuristic heuristic1, PlayerType player2, BoardEvaluationHeuristic heuristic2) {
        return player1.getName() + heuristic1.getName() + "vs" + player2.getName() + heuristic2.getName();
    }

    private IPlayer createPlayer(PlayerType playerType, FieldStatus currentPlayer, BoardEvaluationHeuristic heuristic) {
        switch (playerType) {
            case RANDOM:
                return new RandomPlayer();
            case MIN_MAX_2:
                return new MinMaxPlayer(currentPlayer, 2, heuristic);
            case MIN_MAX_3:
                return new MinMaxPlayer(currentPlayer, 3, heuristic);
            case ALPHA_BETA_2:
                return new AlphaBetaPlayer(currentPlayer, 2, heuristic);
            case ALPHA_BETA_3:
                return new AlphaBetaPlayer(currentPlayer, 3, heuristic);
            default:
                return null;
        }
    }

    public void registerObserver(GameObserver observer) {
        observers.add(observer);
        notifyGameStateChanged();
    }

    public GameStage getCurrentStage() {
        return currentStage;
    }

    public int getActivePlayer() {
        return activePlayer;
    }

    public void fieldSelected(Field field) {
        System.out.println(activePlayer + " " + field.getId());
        if (logger != null) {
            logger.append(activePlayer + " " + field.getId());
        }

        switch (currentStage) {
            case PLACING:
                handlePlacing(field);
                break;
            case MOVING:
                handleMoving(field);
                break;
            case REMOVING:
                handleRemoving(field);
                break;
        }

        if (finishedMoving) {
            if (activePlayer == 1) {
                movesCount++;
            }
            notifyAIFinished();
        }
    }

    public void fieldSelected(int fieldID) {
        fieldSelected(fieldsIDs.get(fieldID));
    }

    public void makeNextMove() {
        if (players.get(activePlayer) != null) {
            List<Move> moves;

            MovesGenerator movesGenerator = new MovesGenerator(convertToBoardState(fields));

            if (currentStage == GameStage.PLACING) {
                moves = movesGenerator.getPlacingMoves(FieldStatus.fromID(activePlayer));
            } else {
                moves = movesGenerator.getMovementMoves(FieldStatus.fromID(activePlayer));
            }

            players.get(activePlayer).makeMove(this, moves);
        }
    }

    public Map<Integer, FieldStatus> getBoardState() {
        return convertToBoardState(fields);
    }

    public int getPlacingsLeft() {
        return fieldsToPlace;
    }

    private void notifyAIFinished() {
        finishedMoving = false;
        observers.forEach(GameObserver::stateChanged);
    }

    private void notifyGameStateChanged() {
        observers.forEach(GameObserver::stateChanged);
    }

    private Map<Integer, FieldStatus> convertToBoardState(Collection<Field> fields) {
        Map<Integer, FieldStatus> result = new HashMap<>();
        fields.forEach(field -> result.put(field.getId(), field.getStatus()));
        return result;
    }

    private void handlePlacing(Field field) {
        if (fieldsToPlace > 0 && field.isFree()) {
            fieldsToPlace--;
            if (fieldsToPlace == 0) {
                currentStage = GameStage.MOVING;
            }
            placePiece(field);
        }

        notifyGameStateChanged();
    }

    private void handleMoving(Field field) {
        if (fieldToMove == null) {
            if (canChooseField(field)) {
                selectFieldsForMovement(field);
            }
        } else {
            if (field.isFree()) {
                if (canFly() || field.isNeighbour(fieldToMove)) {
                    removeSelection();
                    fieldToMove.setStatus(FieldStatus.FREE);
                    fieldToMove = null;
                    placePiece(field);
                }
            } else if (canChooseField(field)) {
                removeSelection();
                selectFieldsForMovement(field);
            }
        }

        notifyGameStateChanged();
    }

    private boolean canFly() {
        return fields.stream().filter(field -> field.getStatus() == FieldStatus.fromID(activePlayer)).count() == 3;
    }

    private void placePiece(Field field) {
        field.setStatus(FieldStatus.fromID(activePlayer));
        if (checkMills(field) && selectFieldsForRemoving()) {
            currentStage = GameStage.REMOVING;
        } else {
            int gameResult = checkGameResult();

            if (gameResult == 1) {
                logResults(1);
                currentStage = GameStage.PLAYER1_WIN;
            } else if (gameResult == 2) {
                logResults(2);
                currentStage = GameStage.PLAYER2_WIN;
            } else {
                changePlayer();
                finishedMoving = true;
            }
        }
    }

    private void changePlayer() {
        if (activePlayer == 1) {
            activePlayer = 2;
        } else {
            activePlayer = 1;
        }
    }

    private boolean checkMills(Field field) {
        return mills.stream().anyMatch(mill -> mill.containsField(field) && mill.isSet());
    }

    private boolean canChooseField(Field field) {
        return field.getStatus() == FieldStatus.fromID(activePlayer);
    }

    private boolean canRemoveField(Field field) {
        return field.getStatus() == FieldStatus.fromID(activePlayer).opposite()
                && mills.stream().noneMatch(mill -> mill.containsField(field) && mill.isSet());
    }

    private void removeSelection() {
        fields.forEach(field -> field.setSelection(FieldSelection.NONE));
    }

    private void selectFieldsForMovement(Field field) {
        fieldToMove = field;
        fieldToMove.setSelection(FieldSelection.SELECTED);
        if (!canFly()) {
            fieldToMove.getNeighbours().forEach(neighbour -> {
                if (neighbour.isFree()) {
                    neighbour.setSelection(FieldSelection.POSSIBLE);
                }
            });
        } else {
            fields.stream().filter(Field::isFree).forEach(f -> f.setSelection(FieldSelection.POSSIBLE));
        }
    }

    private boolean selectFieldsForRemoving() {
        final int[] selectionCount = {0};
        fields.forEach(field -> {
            if (canRemoveField(field)) {
                field.setSelection(FieldSelection.POSSIBLE);
                selectionCount[0] = selectionCount[0] + 1;
            }
        });

        return selectionCount[0] > 0;
    }

    private void handleRemoving(Field field) {
        if (canRemoveField(field)) {
            field.setStatus(FieldStatus.FREE);
            if (fieldsToPlace > 0) {
                currentStage = GameStage.PLACING;
            } else {
                currentStage = GameStage.MOVING;
            }

            removeSelection();

            int gameResult = checkGameResult();

            if (gameResult == 1) {
                logResults(1);
                currentStage = GameStage.PLAYER1_WIN;
            } else if (gameResult == 2) {
                logResults(2);
                currentStage = GameStage.PLAYER2_WIN;
            } else {
                changePlayer();
                finishedMoving = true;
            }

            notifyGameStateChanged();
        }

    }

    private void logResults(int winner) {
        if (logger != null) {
            logger.append(Strings.getPlayer(winner) + " wins");
            logger.append("Moves: " + movesCount);
//            System.out.println("Checked values 1: " + players.get(1).getVisited());
            // TODO - save statistics
            logger.save();
        }
    }

    /**
     * @return 1, 2 - winner; 0 - still playing
     */
    private int checkGameResult() {
        if (currentStage == GameStage.PLACING) {
            return 0;
        }

        if (fields.stream().filter(field -> field.getStatus() == FieldStatus.WHITE).count() == 2) {
            return 2;
        }

        if (fields.stream().filter(field -> field.getStatus() == FieldStatus.BLACK).count() == 2) {
            return 1;
        }

        if (activePlayer == 2 && whiteNoMoves()) {
            return 2;
        }

        if (activePlayer == 1 && blackNoMoves()) {
            return 1;
        }

        return 0;
    }

    private boolean blackNoMoves() {
        return noMoves(FieldStatus.BLACK);
    }

    private boolean whiteNoMoves() {
        return noMoves(FieldStatus.WHITE);
    }

    private boolean noMoves(FieldStatus player) {
        return fields.stream()
                .filter(field -> field.getStatus() == player)
                .map(Field::getNeighbours)
                .allMatch(neighbours -> neighbours.stream().allMatch(f -> f.getStatus() != FieldStatus.FREE));
    }
}
