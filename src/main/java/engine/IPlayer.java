package engine;

import engine.moves.Move;

import java.util.List;

public interface IPlayer {
    void makeMove(GameController engine, List<Move> possibleMoves);

    int getVisited();
}
