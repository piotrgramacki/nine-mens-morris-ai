package engine.logs.impl;

import engine.logs.ILogger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileLogger implements ILogger {
    private String fileName;
    private StringBuilder builder;

    private static final String PATH = "";

    public FileLogger(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void append(String log) {
        if (builder == null) {
            builder = new StringBuilder();
        }

        builder.append(log).append("\n");
    }

    @Override
    public void save() {
        String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss"));

        fileName = fileName + timestamp;

        File file = new File(PATH + fileName + ".log");

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(file))){
            printWriter.println(builder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
