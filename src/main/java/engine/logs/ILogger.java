package engine.logs;

public interface ILogger {
    void append(String log);
    void save();
}
