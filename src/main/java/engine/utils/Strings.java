package engine.utils;

import java.util.List;

public class Strings {
    public static final String APP_NAME = "Nine Men's Morris";

    public static final List<String> PLAYER_TYPES = List.of("Human", "AI (Min-Max-2)", "AI (Min-Max-3)",
            "AI (Alpha-Beta-2)", "AI (Alpha-Beta-3)", "AI (Random)");
    public static final List<String> HEURISTICS = List.of("Mills count", "Fields count", "Fields diff");

    private static final String PLACING_STAGE = "PLACING";
    private static final String MOVING_STAGE = "MOVING";
    private static final String REMOVING_STAGE = "REMOVING";
    private static final String NO_STAGE = "GAME STAGE";
    private static final String PLAYER_1 = "PLAYER 1 WINS";
    private static final String PLAYER_2 = "PLAYER 2 WINS";

    public static String getGameStage(GameStage stage) {
        switch (stage) {
            case PLACING:
                return PLACING_STAGE;
            case MOVING:
                return MOVING_STAGE;
            case REMOVING:
                return REMOVING_STAGE;
            case PLAYER1_WIN:
                return PLAYER_1;
            case PLAYER2_WIN:
                return PLAYER_2;
            default:
                return NO_STAGE;
        }
    }

    // TODO - replace with enum?
    public static String getPlayer(int playerNumber) {
        return "PLAYER " + playerNumber;
    }
}
