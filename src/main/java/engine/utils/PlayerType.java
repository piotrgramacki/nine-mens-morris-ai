package engine.utils;

import java.util.HashMap;

public enum PlayerType {
    HUMAN, MIN_MAX_2, MIN_MAX_3, ALPHA_BETA_2, ALPHA_BETA_3, RANDOM;

    private String name;

    static {
        HUMAN.name = "Human";
        MIN_MAX_2.name = "MinMax2";
        MIN_MAX_3.name = "MinMax3";
        ALPHA_BETA_2.name = "AlphaBeta2";
        ALPHA_BETA_3.name = "AlphaBeta3";
        RANDOM.name = "Random";
    }

    private static HashMap<String, PlayerType> typeMapping = new HashMap<>() {{
        put(Strings.PLAYER_TYPES.get(0), HUMAN);
        put(Strings.PLAYER_TYPES.get(1), MIN_MAX_2);
        put(Strings.PLAYER_TYPES.get(2), MIN_MAX_3);
        put(Strings.PLAYER_TYPES.get(3), ALPHA_BETA_2);
        put(Strings.PLAYER_TYPES.get(4), ALPHA_BETA_3);
        put(Strings.PLAYER_TYPES.get(5), RANDOM);
    }};

    public static PlayerType getPlayerType(String type) {
        return typeMapping.get(type);
    }

    public String getName() {
        return name;
    }
}