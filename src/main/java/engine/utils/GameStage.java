package engine.utils;

public enum GameStage {
    PLACING, MOVING, REMOVING, PLAYER1_WIN, PLAYER2_WIN
}
