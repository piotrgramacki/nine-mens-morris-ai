package engine.utils;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class Colors {
    private static final String BLACK = "#000000";
    private static final String WHITE = "#FFFFFF";
    private static final String EMPTY = "0x00000000";
    private static final String RED = "#FF0000";
    private static final String GREEN = "#00FF00";

    @Contract(pure = true)
    public static String getFieldColor(@NotNull FieldStatus status) {
        switch (status) {
            case BLACK:
                return BLACK;
            case WHITE:
                return WHITE;
            default:
                return EMPTY;
        }
    }

    @Contract(pure = true)
    public static String getSelectionColor(@NotNull FieldSelection selection) {
        switch (selection) {
            case SELECTED:
                return RED;
            case POSSIBLE:
                return GREEN;
            default:
                return EMPTY;
        }
    }
}
