package engine.utils;

// TODO - think about changing this name, not really intuitive
public enum FieldStatus {
    FREE, WHITE, BLACK;

    private FieldStatus opposite;

    static {
        WHITE.opposite = BLACK;
        BLACK.opposite = WHITE;
        FREE.opposite = FREE;
    }

    public static FieldStatus fromID(int id) {
        if (id == 1) {
            return WHITE;
        } else {
            return BLACK;
        }
    }

    public FieldStatus opposite() {
        return opposite;
    }
}
