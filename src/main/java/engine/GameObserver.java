package engine;

public interface GameObserver {
    void stateChanged();
}
