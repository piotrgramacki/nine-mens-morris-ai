package ai.players;

import ai.heuristics.BoardEvaluationHeuristic;
import ai.heuristics.BoardEvaluator;
import engine.GameController;
import engine.IPlayer;
import engine.moves.Move;
import engine.moves.MovesGenerator;
import engine.utils.FieldStatus;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class MinMaxPlayer implements IPlayer {
    private FieldStatus player;
    private int searchDepth;

    private BoardEvaluator evaluator;
    private BoardEvaluationHeuristic heuristic;

    private Move lastMove;

    private int visited = 1;

    public MinMaxPlayer(FieldStatus player, int searchDepth, BoardEvaluationHeuristic heuristic) {
        this.player = player;
        this.searchDepth = searchDepth;
        this.heuristic = heuristic;
    }

    @Override
    public int getVisited() {
        return visited;
    }

    @Override
    public void makeMove(GameController engine, List<Move> possibleMoves) {
        visited++;
        if (evaluator == null) {
            evaluator = new BoardEvaluator(heuristic, player);
        }

        if (possibleMoves.size() > 1 && lastMove != null) {
            possibleMoves.remove(lastMove);
        }

        // prevent making same moves all the time
        Collections.shuffle(possibleMoves);

        Map<Integer, FieldStatus> boardState = engine.getBoardState();
        int placingsLeft = engine.getPlacingsLeft();

        int maxEval = minNode(possibleMoves.get(0).execute(boardState), 1, placingsLeft);
        Move bestMove = possibleMoves.get(0);

        for (int i = 1; i < possibleMoves.size(); i++) {
            if (minNode(possibleMoves.get(i).execute(boardState), 1, placingsLeft) > maxEval) {
                bestMove = possibleMoves.get(i);
            }
        }

        lastMove = bestMove;
        bestMove.execute(engine);

    }

    private int minNode(Map<Integer, FieldStatus> boardState, int currentDepth, int placingsLeft) {
        visited++;
        MovesGenerator movesGenerator = new MovesGenerator(boardState);
        List<Move> movesFrom = placingsLeft > 0 ?
                movesGenerator.getPlacingMoves(player.opposite()) :
                movesGenerator.getMovementMoves(player.opposite());


        if (movesFrom.isEmpty()) {
            return Integer.MAX_VALUE;
        }

        int minValue = maxNode(movesFrom.get(0).execute(boardState),
                currentDepth + 1, placingsLeft - 1);

        for (int i = 1; i < movesFrom.size(); i++) {
            int value = maxNode(movesFrom.get(i).execute(boardState),
                    currentDepth + 1, placingsLeft - 1);
            if (value < minValue) {
                minValue = value;
            }
        }

        return minValue;
    }

    private int maxNode(Map<Integer, FieldStatus> boardState, int currentDepth, int placingsLeft) {
        visited++;
        if (currentDepth > searchDepth) {
            return (int) evaluator.evaluate(boardState);
        }

        MovesGenerator movesGenerator = new MovesGenerator(boardState);
        List<Move> movesFrom = placingsLeft > 0 ?
                movesGenerator.getPlacingMoves(player) :
                movesGenerator.getMovementMoves(player);


        if (movesFrom.isEmpty()) {
            return (-1) * Integer.MAX_VALUE;
        }

        int maxValue = minNode(movesFrom.get(0).execute(boardState), currentDepth, placingsLeft - 1);

        for (int i = 1; i < movesFrom.size(); i++) {
            int value = minNode(movesFrom.get(i).execute(boardState), currentDepth, placingsLeft - 1);
            if (value > maxValue) {
                maxValue = value;
            }
        }

        return maxValue;
    }
}
