package ai.players;

import ai.heuristics.BoardEvaluationHeuristic;
import ai.heuristics.BoardEvaluator;
import engine.GameController;
import engine.IPlayer;
import engine.moves.Move;
import engine.moves.MovesGenerator;
import engine.utils.FieldStatus;

import java.util.List;
import java.util.Map;

public class AlphaBetaPlayer implements IPlayer {
    private FieldStatus player;
    private int searchDepth;

    private BoardEvaluator evaluator;
    private BoardEvaluationHeuristic heuristic;

    private Move lastMove;

    private int visited = 1;

    public AlphaBetaPlayer(FieldStatus player, int searchDepth, BoardEvaluationHeuristic heuristic) {
        this.player = player;
        this.searchDepth = searchDepth;
        this.heuristic = heuristic;
    }

    @Override
    public int getVisited() {
        return visited;
    }

    @Override
    public void makeMove(GameController engine, List<Move> possibleMoves) {
        if (evaluator == null) {
            evaluator = new BoardEvaluator(heuristic, player);
        }

        if (possibleMoves.size() > 1 && lastMove != null) {
            possibleMoves.remove(lastMove);
        }

        visited++;

        Map<Integer, FieldStatus> boardState = engine.getBoardState();
        int placingsLeft = engine.getPlacingsLeft();

        int alpha = (-1) * Integer.MAX_VALUE;
        int beta = Integer.MAX_VALUE;

        // moves ordering heuristic
//        possibleMoves = possibleMoves.stream().sorted((move1, move2) -> {
//            long val1 = evaluator.evaluate(move1.execute(boardState));
//            long val2 = evaluator.evaluate(move2.execute(boardState));
//            return Long.compare(val2, val1);
//        }).collect(Collectors.toUnmodifiableList());

        int maxValue = minNode(possibleMoves.get(0).execute(boardState), 1, placingsLeft, alpha, beta);
        Move bestMove = possibleMoves.get(0);

        if (maxValue > alpha) {
            alpha = maxValue;
        }

        for (int i = 1; i < possibleMoves.size(); i++) {
            int value = minNode(possibleMoves.get(i).execute(boardState), 1, placingsLeft, alpha, beta);
            if (value > maxValue) {
                maxValue = value;
                bestMove = possibleMoves.get(i);

                if (maxValue > alpha) {
                    alpha = maxValue;
                }
            }
        }

        lastMove = bestMove;
        bestMove.execute(engine);

    }

    private int minNode(Map<Integer, FieldStatus> boardState, int currentDepth, int placingsLeft, int alpha, int beta) {
        visited++;
        MovesGenerator movesGenerator = new MovesGenerator(boardState);
        List<Move> movesFrom = placingsLeft > 0 ?
                movesGenerator.getPlacingMoves(player.opposite()) :
                movesGenerator.getMovementMoves(player.opposite());


        if (movesFrom.isEmpty()) {
            return Integer.MAX_VALUE;
        }

        // moves ordering heuristic
//        movesFrom = movesFrom.stream().sorted((move1, move2) -> {
//            long val1 = evaluator.evaluate(move1.execute(boardState));
//            long val2 = evaluator.evaluate(move2.execute(boardState));
//            return Long.compare(val1, val2);
//        }).collect(Collectors.toUnmodifiableList());

        int minValue = maxNode(movesFrom.get(0).execute(boardState),
                currentDepth + 1, placingsLeft - 1, alpha, beta);

        if (minValue < beta) {
            beta = minValue;
        }

        for (int i = 1; beta > alpha && i < movesFrom.size(); i++) {
            int value = maxNode(movesFrom.get(i).execute(boardState),
                    currentDepth + 1, placingsLeft - 1, alpha, beta);
            if (value < minValue) {
                minValue = value;

                if (minValue < beta) {
                    beta = minValue;
                }
            }
        }

        return minValue;
    }

    private int maxNode(Map<Integer, FieldStatus> boardState, int currentDepth, int placingsLeft, int alpha, int beta) {
        visited++;
        if (currentDepth > searchDepth) {
            return (int) evaluator.evaluate(boardState);
        }


        MovesGenerator movesGenerator = new MovesGenerator(boardState);
        List<Move> movesFrom = placingsLeft > 0 ?
                movesGenerator.getPlacingMoves(player) :
                movesGenerator.getMovementMoves(player);


        if (movesFrom.isEmpty()) {
            return (-1) * Integer.MAX_VALUE;
        }

        // moves ordering heuristic
//        movesFrom = movesFrom.stream().sorted((move1, move2) -> {
//            long val1 = evaluator.evaluate(move1.execute(boardState));
//            long val2 = evaluator.evaluate(move2.execute(boardState));
//            return Long.compare(val2, val1);
//        }).collect(Collectors.toUnmodifiableList());

        int maxValue = minNode(movesFrom.get(0).execute(boardState), currentDepth, placingsLeft - 1,
                alpha, beta);

        if (maxValue > alpha) {
            alpha = maxValue;
        }

        for (int i = 1; alpha < beta && i < movesFrom.size(); i++) {
            int value = minNode(movesFrom.get(i).execute(boardState), currentDepth, placingsLeft - 1,
                    alpha, beta);
            if (value > maxValue) {
                maxValue = value;

                if (maxValue > alpha) {
                    alpha = maxValue;
                }
            }
        }

        return maxValue;
    }
}
