package ai.players;

import engine.GameController;
import engine.IPlayer;
import engine.moves.Move;

import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * This implementation performs random move with priority to moves which lead to removing opponent's piece
 */
public class RandomPlayer implements IPlayer {
    private Random random = new Random(13);

    @Override
    public void makeMove(GameController engine, List<Move> possibleMoves) {
        int size = possibleMoves.size();

        Optional<Move> removingMove = possibleMoves.stream().filter(Move::removes).findAny();

        if (removingMove.isPresent()) {
            removingMove.get().execute(engine);
        } else {
            possibleMoves.get(random.nextInt(size)).execute(engine);
        }
    }

    @Override
    public int getVisited() {
        return 0;
    }
}
