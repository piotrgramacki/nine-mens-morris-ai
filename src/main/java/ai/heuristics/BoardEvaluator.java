package ai.heuristics;

import board.Mill;
import board.utils.BoardCreator;
import engine.utils.FieldStatus;

import java.util.Map;
import java.util.Set;

public class BoardEvaluator {
    private BoardEvaluationHeuristic heuristic;
    private FieldStatus currentPlayer;

    public BoardEvaluator(BoardEvaluationHeuristic heuristic, FieldStatus currentPlayer) {
        this.heuristic = heuristic;
        this.currentPlayer = currentPlayer;
    }

    public long evaluate(Map<Integer, FieldStatus> boardState) {
        switch (heuristic) {
            case MILLS_COUNT:
                return millsCountHeuristic(boardState);
            case FIELDS_COUNT:
                return fieldsCountHeuristic(boardState);
            case FIELDS_DIFF:
                return fieldsDiffHeuristic(boardState);
            default:
                return 0;
        }
    }

    private long fieldsDiffHeuristic(Map<Integer, FieldStatus> boardState) {
        int diff = 0;

        for (Map.Entry<Integer, FieldStatus> entry: boardState.entrySet()){
            if (entry.getValue() == currentPlayer) {
                diff++;
            } else if (entry.getValue() == currentPlayer.opposite()) {
                diff--;
            }
        }

        return diff;
    }

    private long fieldsCountHeuristic(Map<Integer, FieldStatus> boardState) {
        return boardState.values().stream().filter(field -> field == currentPlayer).count();
    }

    private long millsCountHeuristic(Map<Integer, FieldStatus> boardState) {
        Set<Mill> mills = BoardCreator.getMills();

        return mills.stream().filter(mill -> mill.isSet(boardState, currentPlayer)).count();
    }
}
