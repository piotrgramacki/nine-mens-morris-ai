package ai.heuristics;

import engine.utils.Strings;

import java.util.HashMap;

public enum BoardEvaluationHeuristic {
    MILLS_COUNT, FIELDS_COUNT, FIELDS_DIFF;

    private String name;

    static {
        MILLS_COUNT.name = "(mills_count)";
        FIELDS_COUNT.name = "(fields_count)";
        FIELDS_DIFF.name = "(fields_diff)";
    }

    private static HashMap<String, BoardEvaluationHeuristic> typeMapping = new HashMap<>() {{
        put(Strings.HEURISTICS.get(0), MILLS_COUNT);
        put(Strings.HEURISTICS.get(1), FIELDS_COUNT);
        put(Strings.HEURISTICS.get(2), FIELDS_DIFF);
    }};

    public static BoardEvaluationHeuristic getHeuristic(String type) {
        return typeMapping.get(type);
    }

    public String getName() {
        return name;
    }
}
